import { Box, Button, TextField } from '@mui/material'
import React, { ChangeEvent, useContext, useState } from 'react'
import SaveIcon from '@mui/icons-material/Save';
import ControlPointIcon from '@mui/icons-material/ControlPoint';
import { EntriesContext } from '../../context/entries/EntriesContext';
import { UIContext } from '../../context/ui';

export const NewEntry = () => {

  const {isAdding, setIsAddingEntry} = useContext(UIContext);

  const [inputValue, setInputValue] = useState('');

  const [touched, setTouched] = useState(false);

  const {addNewEntry} = useContext(EntriesContext);

  const onTextFieldChanges = (event: ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  }

  const onSave = () => {
    if (inputValue.length === 0) return;
    addNewEntry(inputValue);
    setIsAddingEntry(false);
    setTouched(false);
    setInputValue('');
  }

  return (
    <Box sx={{marginBottom: 2, paddingX: 2}}>

      {
        isAdding ? (
          <>
            <TextField fullWidth 
              sx={{marginTop: 2, marginBottom: 1}}
              placeholder='Nueva entrada'
              autoFocus
              multiline
              label='Nueva entrada'
              helperText={inputValue.length <= 0 && touched && 'Ingrese un valor'}
              error={inputValue.length <= 0 && touched}
              value={inputValue}
              onChange={onTextFieldChanges}
              onBlur={() => setTouched(true)}
            />
            <Box display='flex' justifyContent='space-between'>
              <Button variant='text'
                onClick={() => setIsAddingEntry(false)}
              >
                  Cancelar
              </Button>
              <Button variant='outlined'
                  color='secondary'
                  endIcon={<SaveIcon />}
                  onClick={onSave}
              >
                Guardar
              </Button>
            </Box>
          </>
        )

        : (
          <Button startIcon={<ControlPointIcon />}
              fullWidth
              variant='outlined'
              onClick={() => setIsAddingEntry(true)}
            >
              Agregar tarea
            </Button>
        )
      }

      
    </Box>
  )
}
