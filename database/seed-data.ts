

interface SeedData {
    entries: SeedEntry[]
}

interface SeedEntry {
    description: string;
    status: string;
    createdAt: number;
}

export const seedData: SeedData = {
    entries: [
        {
            description: 'Pendiente: Nisi excepteur proident mollit reprehenderit sit dolore tempor ea officia et consectetur nostrud cupidatat quis.',
            status: 'pending',
            createdAt: Date.now(),
        },
        {
            description: 'En progreso: Nisi excepteur proident mollit reprehenderit sit dolore tempor ea officia et consectetur nostrud cupidatat quis.',
            status: 'in-progress',
            createdAt: Date.now() - 1000000,
        },
        {
            description: 'Terminada: Nisi excepteur proident mollit reprehenderit sit dolore tempor ea officia et consectetur nostrud cupidatat quis.',
            status: 'finished',
            createdAt: Date.now() - 100000,
        },
    ]
}