import { createContext } from 'react';

interface ContextProps {
    sideMenuOpen: boolean;
    isAdding: boolean;
    isDragging: boolean;

    // Methods
    openSideMenu: () => void;
    closeSideMenu: () => void;
    setIsAddingEntry: (isAdding: boolean) => void
    startDragging: () => void
    endDragging: () => void
} 

export const UIContext = createContext({} as ContextProps); 