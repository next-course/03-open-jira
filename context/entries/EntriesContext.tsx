import { createContext } from 'react';
import { Entry } from '../../interfaces';

interface ContextProps {
    entries: Entry[];
    updateEntry: (entry: Entry, showSnackBar: Boolean) => void;
    // Methods
    addNewEntry: (description: string) => void
    deleteEntry: (entry: Entry, showSnackBar: Boolean) => void;
} 

export const EntriesContext = createContext({} as ContextProps);